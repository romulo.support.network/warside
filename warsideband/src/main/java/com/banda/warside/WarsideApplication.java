package com.banda.warside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarsideApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarsideApplication.class, args);
	}

}
